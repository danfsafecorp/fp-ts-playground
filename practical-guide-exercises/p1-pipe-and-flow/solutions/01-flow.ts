/**
 * Be sure to read part 1 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-1
 *
 * Exercise:
 *  - Modify goodDayMany to use flow instead of pipe
 */

import { flow } from 'fp-ts/lib/function'
import { GreetingFunction } from './00-pipe'

const hello = (name: string) => `Hello ${name}!`
const appendGoodDay = (str: string) => `${str}Good day!`
const appendSpace = (str: string) => str + ' '

const greetMany = (names: string[], greetingFunction: GreetingFunction) =>
  names.map(greetingFunction)

// Modify this function to use flow instead of pipe
export const goodDayMany = (names: string[]) =>
  greetMany(names, flow(hello, appendSpace, appendGoodDay))
