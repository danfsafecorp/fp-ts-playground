/**
 * Exercise
 * - Update findProductOfColor to return an O.Option<Product> with the first Product matching the input color if available
 * - Update findProductOfColor to return a Product that is the first Product matching the input color if available, otherwise returning the DefaultProduct
 */

import { pipe } from 'fp-ts/function'
import * as O from 'fp-ts/Option'
import * as A from 'fp-ts/Array'

type Color = 'RED' | 'BLUE' | 'GREEN'

export interface Product {
  id: string
  color: Color
}

export const DefaultProduct: Product = {
  id: 'DEFAULT',
  color: 'RED',
}

export const findProductOfColor = (products: Product[], color: Color): O.Option<Product> =>
  pipe(
    products,
    A.findFirst(product => product.color === color)
  )

export const findProductOfColorWithDefault = (products: Product[], color: Color): Product =>
  pipe(
    products,
    A.findFirst(product => product.color === color),
    O.getOrElse(() => DefaultProduct)
  )
