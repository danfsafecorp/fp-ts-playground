/**
 * Exercise:
 * - Implement the following types using io-ts
 * - CardBase should be implemented as Card
 * - DisplayCardBase should be implemented as DisplayCard
 */
import * as t from 'io-ts'
import { CardNetwork, CardStatus } from './card-api'

type AddressBase = {
  first_name: string
  last_name: string
  address_1: string
  address_2?: string
  city: string
  state: string
  zip: string
  country?: string
}

type CardBase = {
  id: string
  status: CardStatus
  network?: CardNetwork
  last4?: string
  fulfillment?: {
    recipient_address?: AddressBase
    recipient_mailing_address?: AddressBase
  }
}

type DisplayCardBase = {
  id: string
  status: CardStatus
  network: string
  display_name: string
  mail_to_address?: AddressBase
}

// TODO: Implementation here
export const Card = {}
export const DisplayCard = {}

// No need to modify below here
export type Card = t.TypeOf<typeof Card>
export type DisplayCard = t.TypeOf<typeof DisplayCard>

export const CardArray = t.array(Card)
export type CardArray = t.TypeOf<typeof CardArray>
