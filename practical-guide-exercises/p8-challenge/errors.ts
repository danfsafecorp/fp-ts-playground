export class CardsNotFoundError extends Error {
  public readonly type = 'CARDS_NOT_FOUND'
}

export class GetCardNetworkError extends Error {
  public readonly type = 'GET_CARD_NETWORK'
}

export class IoTsValidationError extends Error {
  public readonly type = 'IO_TS_VALIDATION'
}

export class NoValidCardsFoundError extends Error {
  public readonly type = 'NO_VALID_CARDS_FOUND'
}

export type CardErrors =
  | CardsNotFoundError
  | GetCardNetworkError
  | IoTsValidationError
  | NoValidCardsFoundError
