import { GetCards } from '../card-api'
import * as TE from 'fp-ts/TaskEither'
import * as NEA from 'fp-ts/NonEmptyArray'
import * as E from 'fp-ts/Either'
import { CardsNotFoundError, GetCardNetworkError, IoTsValidationError } from '../errors'
import { Card } from './01-types'
import { pipe } from 'fp-ts/lib/function'
import { CardArray } from './01-types'

export const getAndParseCards = (getCards: GetCards) => (
  userId: string
): TE.TaskEither<GetCardNetworkError | CardsNotFoundError | IoTsValidationError, Card[]> =>
  pipe(
    getCards(userId),
    TE.mapLeft(() => new GetCardNetworkError()),
    TE.chainW(cards =>
      pipe(
        NEA.fromArray(cards),
        TE.fromOption(() => new CardsNotFoundError())
      )
    ),
    TE.chainW(cards =>
      pipe(
        cards,
        CardArray.decode,
        E.mapLeft(() => new IoTsValidationError()),
        TE.fromEither
      )
    )
  )
