/**
 * Be sure to read part 5 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-5
 * 
 * Note: The above guide uses a deprecated method for sequencing -- use T.sequenceArray instead of A.array.sequence(T.task)
 *
 * Exercise:
 *  - Modify add5Sequence to fix the type error by adding a task sequence
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 00-sequence.ts
 *
 * Testing this file.
 *  - npm run test 00-sequence
 */

 import { pipe } from 'fp-ts/lib/function'
 import * as T from 'fp-ts/lib/Task'
 import * as A from 'fp-ts/lib/Array'
 import { runIfCli } from '../utils'
 
 const add5Task = (num: number): T.Task<number> => async () => num + 5
 
 export const add5Sequence = (numArr: number[]): T.Task<readonly number[]> =>
   pipe(
     numArr,
     A.map(add5Task),
     // TODO add a Task sequence here to resolve the type issue
   )
 
// No need to modify below here, for running this file
const logAndReturnResult = (numArr: number[]) =>
  pipe(
    add5Sequence(numArr),
    T.map(result =>
      pipe(
        console.dir(`add5Sequence([${numArr.join(',')}]):`),
        () => console.dir(result),
        () => result
      )
    )
  )

pipe(logAndReturnResult([1, 2, 3]), runIfCli(module))
 