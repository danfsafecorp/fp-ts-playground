import * as t from 'io-ts'

interface TransactionBase {
  transaction_id: string
  amount: number
  fee_amount?: number
  origin: {
    account_number: string
    routing_number: string
    alias?: string
  }
  receiver: {
    account_number: string
    routing_number: string
    alias?: string
  }
  metadata?: {
    with_fee: boolean
    fee_type?: string
    validated: boolean
    validation_details?: {
      validated_by?: string
      validate_date?: string
    }
  }
}

export const Transaction = t.intersection([
  t.type({
    transaction_id: t.string,
    amount: t.number,
    origin: t.intersection([
      t.type({
        account_number: t.string,
        routing_number: t.string,
      }),
      t.partial({
        alias: t.string,
      }),
    ]),
    receiver: t.intersection([
      t.type({
        account_number: t.string,
        routing_number: t.string,
      }),
      t.partial({
        alias: t.string,
      }),
    ]),
  }),
  t.partial({
    fee_amount: t.number,
    metadata: t.intersection([
      t.type({
        with_fee: t.boolean,
        validated: t.boolean,
      }),
      t.partial({
        fee_type: t.string,
        validation_details: t.partial({
          validated_by: t.string,
          validate_date: t.string,
        }),
      }),
    ]),
  }),
])

export type Transaction = t.TypeOf<typeof Transaction>

// Alternate solution using additional types

const Account = t.intersection([
  t.type({
    account_number: t.string,
    routing_number: t.string,
  }),
  t.partial({
    alias: t.string,
  }),
])

const ValidationDetails = t.partial({
  validated_by: t.string,
  validate_date: t.string,
})

const Metadata = t.intersection([
  t.type({
    with_fee: t.boolean,
    validated: t.boolean,
  }),
  t.partial({
    fee_type: t.string,
    validation_details: ValidationDetails,
  }),
])

const TransactionRequired = t.type({
  transaction_id: t.string,
  amount: t.number,
  origin: Account,
  receiver: Account,
})

const TransactionOptional = t.partial({
  fee_amount: t.number,
  metadata: Metadata,
})

export const TransactionAlternate = t.intersection([TransactionRequired, TransactionOptional])
