import { User, Account } from './00-strict-and-type'
import * as E from 'fp-ts/Either'
describe('strict-and-type', () => {
  describe('User', () => {
    it('handles an invalid input', () => {
      const input = {
        definitely: 'not',
        a: 'user',
      }
      expect(User.is(input)).toBe(false)
    })

    it('handles a valid input', () => {
      const input = {
        user_id: '1',
        first_name: 'Malcolm',
        last_name: 'McCormick',
        email: 'stub@mock.com',
        age: 26,
        address: {
          address_1: '2407 J St',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
      }
      expect(User.is(input)).toBe(true)
      expect(User.decode(input)).toStrictEqual(
        E.right({
          user_id: '1',
          first_name: 'Malcolm',
          last_name: 'McCormick',
          email: 'stub@mock.com',
          age: 26,
          address: {
            address_1: '2407 J St',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        })
      )
    })

    it('handles a valid input with additional data', () => {
      const input = {
        user_id: '1',
        first_name: 'Malcolm',
        last_name: 'McCormick',
        email: 'stub@mock.com',
        age: 26,
        address: {
          address_1: '2407 J St',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
        phone_number: '+1238675309',
        delinquent: false,
      }
      expect(User.is(input)).toBe(true)
      expect(User.decode(input)).toStrictEqual(
        E.right({
          user_id: '1',
          first_name: 'Malcolm',
          last_name: 'McCormick',
          email: 'stub@mock.com',
          age: 26,
          address: {
            address_1: '2407 J St',
            city: 'Sacramento',
            state: 'CA',
            zip: '95816',
            country: 'USA',
          },
        })
      )
    })
  })

  describe('Account', () => {
    it('handles an invalid input', () => {
      const input = {
        definitely: 'not',
        a: 'account',
      }
      expect(Account.is(input)).toBe(false)
    })

    it('handles a valid input', () => {
      const input = {
        account_id: '1',
        user_id: '1',
        account_numbers: [1, 2, 3],
        is_verified: true,
      }
      expect(Account.is(input)).toBe(true)
      expect(Account.decode(input)).toStrictEqual(
        E.right({
          account_id: '1',
          user_id: '1',
          account_numbers: [1, 2, 3],
          is_verified: true,
        })
      )
    })

    it('handles a valid input with additional data', () => {
      const input = {
        account_id: '1',
        user_id: '1',
        account_numbers: [1, 2, 3],
        metadata: {
          create_date: '2022-09-01',
        },
        is_verified: true,
        phone: '+1238675309',
        email: 'stub@mock.com',
      }
      expect(Account.is(input)).toBe(true)
      expect(Account.decode(input)).toStrictEqual(
        E.right({
          account_id: '1',
          user_id: '1',
          account_numbers: [1, 2, 3],
          metadata: {
            create_date: '2022-09-01',
          },
          is_verified: true,
          phone: '+1238675309',
          email: 'stub@mock.com',
        })
      )
    })
  })
})
