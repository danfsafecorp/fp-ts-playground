import * as A from 'fp-ts/lib/Array'
import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'

const getUser = (userId: string): T.Task<string> => async () => userId

const getUsers = (userIds: string[]) => A.array.traverse(T.task)(userIds, getUser)

const task = pipe(
  getUser('user.345'),
  T.bindTo('first'),
  T.chain(() => getUser('user.456')),
  T.chain(() => getUsers(['user.123', 'user.234'])),
  T.map(users => console.dir(users))
)

task()
